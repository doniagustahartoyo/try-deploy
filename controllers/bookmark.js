const { bookmark, event, user, category } = require("../models");

class Bookmark {
  async getAllBookmark(req, res, next) {
    try {
      const userId = req.userData.id;

      const data = await bookmark.findAll({
        where: {
          id_user: +userId,
        },
        attributes: {
          exclude: ["id_event", "id_user", "createdAt", "updatedAt", "deletedAt"],
        },
        include: [
          {
            model: user,
            attributes: ["email"],
          },
          {
            model: event,
            attributes: ["title"],
          },
          {
            model: event,
            attributes: ["image"],
          },
          {
            model: event,
            attributes: ["id_category"],
            include: {
              model: category,
            },
          },
        ],
        order: [["createdAt", "DESC"]],
      });

      if (data.length == 0) {
        return res.status(200).json({ success: true, message: ["No Saved Bookmark"] });
      }

      res.status(200).json({ success: true, data: data });
    } catch (error) {
      res.status(500).json({ success: false, errors: ["Internal Server Error"] });
    }
  }

  async addBookmark(req, res, next) {
    try {
      const userId = req.userData.id;
      const { idEvent } = req.params;

      if (idEvent) {
        const checkBookmark = await bookmark.findAll({
          where: {
            id_user: +userId,
            id_event: +idEvent,
          },
        });
        if (checkBookmark.length != 0) {
          res.status(500).json({ success: false, errors: ["Bookmark already added"] });
        } else {
          const newData = await bookmark.create({
            id_user: +userId,
            id_event: +idEvent,
          });

          const data = await bookmark.findOne({
            where: {
              id: newData.id,
            },
            attributes: {
              exclude: ["id_event", "id_user", "createdAt", "updatedAt", "deletedAt"],
            },
            include: [
              {
                model: user,
                attributes: ["first_name"],
              },
              {
                model: user,
                attributes: ["last_name"],
              },
              {
                model: user,
                attributes: ["email"],
              },
              {
                model: event,
                attributes: ["event_title"],
              },
            ],
          });
          res.status(201).json({ success: true, data: data });
        }
      }
    } catch (error) {
      res.status(500).json({ success: false, errors: ["Internal Server Error"] });
    }
  }

  async deleteBookmark(req, res, next) {
    try {
      const { idBookmark } = req.params;
      const deletedData = await bookmark.destroy({
        where: {
          id: +idBookmark,
        },
        force: true,
      });

      res.status(200).json({ success: true, message: ["Bookmark removed"] });
    } catch (error) {
      res.status(500).json({ success: false, errors: ["Internal Server Error"] });
    }
  }
}

module.exports = new Bookmark();
