const { user, speaker, category, event, comment, bookmark } = require("../models"); //Import models
const { Op } = require("sequelize"); //Import sequelize
const moment = require("moment-timezone"); //Import moment
const { image } = require("faker");

class Event {
  //Get list of event
  static async getEventList(req, res, next) {
    try {
      let page = req.query.page;
      let limit = parseInt(req.query.limit) || 4;

      let offset = (+page - 1) * parseInt(limit);
      let data = await event.findAll({
        offset,
        limit,
        where: {},
        attributes: {
          exclude: ["detail", "id_bookmark", "linkMeet", "createdAt", "updateAt", "deletedAd", "id_speaker", "id_user", "id_category"],
        },
        include: [
          {
            model: speaker,
            attributes: ["image", "name"],
          },
          {
            model: category,
            attributes: ["name"],
          },
        ],
      });
      if (data.length === 0) {
        return res.status(404).json({ errors: ["Maaf, tidak ada event apapun."] });
      }
      res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // Get Detail of Events
  static async getDetailEvent(req, res, next) {
    try {
      const data = await event.findOne({
        where: { id: req.params.id },
        attributes: {
          exclude: ["id_user", "createdAt", "updateAt", "deletedAt"],
        },
        include: [
          {
            model: speaker,
            attributes: {
              exclude: ["createdAt", "updatedAt", "deletedAt"],
            },
          },
          {
            model: category,
            attributes: {
              exclude: ["createdAt", "updatedAt", "deletedAt"],
            },
          },
          {
            model: comment,
            attributes: {
              exclude: ["createdAt", "updatedAt", "deletedAt"],
            },
          },
        ],
      });
      if (data === null) {
        return res.status(404).json({ errors: ["Event yang kamu cari tidak ditemukan."] });
      }
      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  static async getEventByTitle(req, res, next) {
    try {
      const title = req.query.title;
      const findOptions = {};

      if (title && title != "") {
        findOptions.where = {
          title: {
            [Op.like]: `%${title}%`,
          },
        };
      }

      const events = await event.findAll(findOptions);

      if (events.length === 0) {
        return res.status(404).json({ errors: ["Events not found"] });
      }

      res.status(200).json({ events });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
  static async getEventByCategory(req, res, next) {
    try {
      const categories = req.query.category;
      const findOptions = {
        include: [],
      };

      if (categories && categories != "") {
        findOptions.include.push({
          model: category,
          where: {
            name: {
              [Op.like]: `%${categories}%`,
            },
          },
        });
      }
      const events = await event.findAll(findOptions);

      if (events.length === 0) {
        return res.status(404).json({ errors: ["Events not found"] });
      }

      res.status(200).json({ events });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // filter this week event
  static async getThisWeekEvent(req, res, next) {
    try {
      let page = req.query.page;
      let limit = parseInt(req.query.limit) || 4;

      let offset = (+page - 1) * parseInt(limit);

      let a = new Date();
      console.log(a);

      const where = {
        date: {
          [Op.between]: [moment().startOf("week").format(), moment().add(6, "week").format()],
        },
      };

      let data = await event.findAll({
        where,
        attributes: ["image", "date", "title"],
        include: [
          { model: speaker, attributes: ["name", "image"] },
          { model: category, attributes: ["name"] },
        ],
        limit,
        offset,

        order: [["date", "DESC"]],
      });
      if (data.length === 0) {
        return res.status(404).json({ errors: ["Event yang kamu cari tidak ditemukan"] });
      }

      return res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // get this month event
  static async getThisMonthEvent(req, res, next) {
    try {
      let page = req.query.page;
      let limit = parseInt(req.query.limit) || 4;

      let offset = (+page - 1) * parseInt(limit);

      let data = await event.findAll({
        where: {
          date: {
            [Op.between]: [moment().startOf("month").format(), moment().endOf("month").format()],
          },
        },
        attributes: ["image", "date", "title"],
        include: [
          { model: speaker, attributes: ["name", "image"] },
          { model: category, attributes: ["name"] },
        ],
        limit,
        offset,

        order: [["date", "ASC"]],
      });

      if (data.length === 0) {
        return res.status(404).json({ errors: ["Event yang kamu cari tidak ditemukan"] });
      }

      return res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  // get this year event
  static async getThisYearEvent(req, res, next) {
    try {
      let page = req.query.page;
      let limit = parseInt(req.query.limit) || 4;

      let offset = (+page - 1) * parseInt(limit);

      let data = await event.findAll({
        where: {
          date: {
            [Op.between]: [moment().startOf("year").format(), moment().endOf("year").format()],
          },
        },
        attributes: ["image", "date", "title"],
        include: [
          { model: speaker, attributes: ["name", "image"] },
          { model: category, attributes: ["name"] },
        ],
        limit,
        offset,

        order: [["date", "ASC"]],
      });

      if (data.length === 0) {
        return res.status(404).json({ errors: ["Event yang kamu cari tidak ditemukan"] });
      }

      return res.status(200).json({ data });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }

  //Create New Event
  static async createNewEvent(req, res, next) {
    try {
      // Make create Event
      const newData = await event.create(req.body);
      // Find Event with join
      const data = await event.findOne({
        where: {
          id: newData.id,
        },
        attributes: { exclude: ["id_speaker", "id_user", "id_category"] },
        include: [
          {
            model: user,
            attributes: { exclude: ["password", "email"] },
          },
          {
            model: speaker,
          },
          // {
          //   model: boorkmark,
          // },
          {
            model: category,
          },
        ],
      });

      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json(error.message);
    }
  }

  static async updateEvent(req, res, next) {
    try {
      const updateEvent = await event.update(
        {
          title: req.body.title,
          image: req.body.image,
          detail: req.body.detail,
          date: req.body.date,
          linkMeet: req.body.linkMeet,
          id_speaker: req.body.id_speaker,
          id_user: req.body.id_user,
          id_bookmark: req.body.id_bookmark,
          id_category: req.body.id_category,
        },
        { where: { id: req.params.id } }
      );
      const data = await event.findOne({
        where: { id: req.params.id },
      });

      res.status(201).json({
        status: 201,
        message: "Event berhasil diupdate",
        data,
      });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
  //Delete Event
  static async deleteEvent(req, res, next) {
    try {
      let data = await event.destroy({
        where: { id: req.params.id },
      });
      res.status(201).json({ data, message: "Event berhasil dihapus" });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
}

module.exports = Event;
