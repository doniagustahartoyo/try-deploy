const { user } = require("../models");
const Encryption = require("../utils/encryption");

class Users {
  static async create(req, res, next) {
    try {
      const firstname = req.body.firstname;
      const lastname = req.body.lastname;
      const email = req.body.email;
      const password = Encryption.encryptPass(req.body.password);
      const objUser = { firstname, lastname, email, password };
      console.log(objUser);

      const newUser = await user.create(objUser);

      const User = await user.findOne({
        where: {
          id: newUser.id,
        },
        attributes: { exclude: ["password"] },
      });

      res.status(201).json({ User });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = Users;
