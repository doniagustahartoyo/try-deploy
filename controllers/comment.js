const { comment, user, event } = require("../models");

class Comment {
  // Create Comment
  static async createComment(req, res, next) {
    try {
      // Create comment
      const newData = await comment.create(req.body);
      // Find comment
      const data = await comment.findOne({
        where: {
          id: newData.id,
        },
        attributes: { exclude: ["id_user", "id_event", "updatedAt", "deletedAt"] },
        include: [{ model: user }, { model: event, attributes: ["title"] }],
      });
      res.status(201).json({ data });
    } catch (error) {
      console.log(error);
      res.status(500).json(error.message);
    }
  }
  //Get all comments
  static async getAllComments(req, res, next) {
    try {
      const data = await comment.findAll({
        where: {},
        attributes: { exclude: ["createdAt", "updatedAt", "deletedAt"] },
        include: [
          {
            model: user,
            attributes: ["firstname", "lastname"],
          },
        ],
      });
      res.status(201).json({ data });
    } catch (error) {
      res.status(500).json(error.message);
    }
  }
  // Update Comment
  static async updateComment(req, res, next) {
    try {
      const updatedComment = await comment.update(req.body, {
        where: {
          id: req.params.id,
        },
      });
      if (updatedComment[0] === 0) {
        return res.status(404).json({ errors: ["Komentar anda gagal diperbarui!"] });
      }

      const data = await comment.findOne({
        where: {
          id: req.params.id,
        },
        attributes: ["comment", "createdAt"],
        include: [
          {
            model: user,
            attributes: ["firstname", "lastname"],
          },
        ],
      });

      res.status(201).json({ data, message: ["Komentar Anda Berhasil diperbarui."] });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
  // Delete Comment
  static async deleteComment(req, res, next) {
    try {
      let data = await comment.destroy({ where: { id: req.params.id } });

      if (!data) {
        return res.status(404).json({ errors: ["Komentar tidak ditemukan!"] });
      }

      res.status(200).json({ message: "Komentar Anda Berhasil Dihapus." });
    } catch (error) {
      res.status(500).json({ errors: ["Internal Server Error"] });
    }
  }
}

module.exports = Comment;
