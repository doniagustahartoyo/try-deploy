const {
  bookmark,
  category,
  comment,
  event,
  speaker,
  user,
  sequelize,
} = require("../models");
const request = require("supertest");
const app = require("../index");

beforeAll(() => {
  user.create({
    firstname: "Muhammad",
    lastname: "Yusril",
    email: "yusril@gmail.com",
    password: "rahasia",
  });
});
afterAll((done) => {
  user
    .destroy({ where: {} })
    .then(() => {
      done();
    })
    .catch((err) => {
      done(err);
    });
});

describe("User try to login:", () => {
  describe("Success:", () => {
    it("Should return 200 and access_token", (done) => {
      let input = {
        email: "yusril@gmail.com",
        password: "rahasia",
      };
      request(app)
        .post("/signin")
        .send(input)
        .then((response) => {
          let { body, status } = response;
          expect(status).toBe(200);
          console.log("user's token", body.access_token);
          expect(body).toHaveProperty("access_token");
          expect(typeof body.access_token).toBe("string");
          done();
        })
        .catch((err) => {
          done(err);
        });
    });
  });
});
