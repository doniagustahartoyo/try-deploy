"use strict";
const faker = require("faker");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("events", [
      {
        title: faker.animal.snake(),
        image: faker.image.animals(),
        detail: "a",
        date: faker.date.past(),
        linkMeet: faker.internet.url(),
        id_speaker: 1,
        id_user: 1,
        id_bookmark: 1,
        id_category: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: faker.animal.snake(),
        image: faker.image.animals(),
        detail: "b",
        date: faker.date.past(),
        linkMeet: faker.internet.url(),
        id_speaker: 2,
        id_user: 2,
        id_bookmark: 2,
        id_category: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: faker.animal.snake(),
        image: faker.image.animals(),
        detail: "c",
        date: faker.date.past(),
        linkMeet: faker.internet.url(),
        id_speaker: 3,
        id_user: 3,
        id_bookmark: 3,
        id_category: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: faker.animal.snake(),
        image: faker.image.animals(),
        detail: "d",
        date: faker.date.past(),
        linkMeet: faker.internet.url(),
        id_speaker: 4,
        id_user: 4,
        id_bookmark: 4,
        id_category: 4,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: faker.animal.snake(),
        image: faker.image.animals(),
        detail: "e",
        date: faker.date.past(),
        linkMeet: faker.internet.url(),
        id_speaker: 5,
        id_user: 5,
        id_bookmark: 5,
        id_category: 5,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("categories", null, {});
  },
};
