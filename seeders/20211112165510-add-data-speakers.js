"use strict";
const faker = require("faker");

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert("speakers", [
      {
        name: faker.name.findName(),
        image: faker.image.imageUrl(),
        id_event: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: faker.name.findName(),
        image: faker.image.imageUrl(),
        id_event: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: faker.name.findName(),
        image: faker.image.imageUrl(),
        id_event: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: faker.name.findName(),
        image: faker.image.imageUrl(),
        id_event: 4,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: faker.name.findName(),
        image: faker.image.imageUrl(),
        id_event: 5,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete("speakers", null, {});
  },
};
