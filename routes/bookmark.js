const express = require("express");

const { getAllBookmark, createBookmark, updateBookmark, deleteBookmark } = require("../controllers/bookmark");

const router = express.Router();

router.get("/", getAllBookmark);
router.post("/", createBookmark);
router.put("/:id", updateBookmark);
router.delete("/:id", deleteBookmark);

module.exports = router;
