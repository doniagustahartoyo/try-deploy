const Event = require("../controllers/event");
const { create } = require("../controllers/user");
const router = require("express").Router();
const { createValidatorEvent } = require("../middlewares/validators/createEvent");

router.get("/", Event.getEventList);

router.get("/title", Event.getEventByTitle);
router.get("/category", Event.getEventByCategory);
// router.get("/today", Event.getTodayEvent);
// router.get("/tomorrow", Event.getTomorrowEvent);
router.get("/week", Event.getThisWeekEvent);
router.get("/month", Event.getThisMonthEvent);
router.get("/year", Event.getThisYearEvent);
router.post("/", createValidatorEvent, Event.createNewEvent);
router.put("/:id", Event.updateEvent);
router.delete("/:id", Event.deleteEvent);
router.get("/:id", Event.getDetailEvent);

module.exports = router;
