const router = require("express").Router();
const eventRoute = require("./event");
const commentRoute = require("./comment");
// const bookmarkRoute = require("./bookmark");

const SignIn = require("../controllers/signIn");
const Users = require("../controllers/user");
const { createUserValidator } = require("../middlewares/validators/user");
// const Auth = require("../middlewares/auth");

router.use("/comment", commentRoute);
router.use("/event", eventRoute);
// router.use("/bookmark", bookmarkRoute);

router.post("/signin", SignIn.signIn);
router.post("/signup", createUserValidator, Users.create);

module.exports = router;
