const express = require("express"); // Import Express

const { createCommentValidator } = require("../middlewares/validators/comment"); //Import validator

const { getAllComments, createComment, updateComment, deleteComment } = require("../controllers/comment");

const router = express.Router();

router.get("/", getAllComments);
router.post("/", createCommentValidator, createComment);
router.put("/:id", createCommentValidator, updateComment);
router.delete("/:id", deleteComment);

module.exports = router;
