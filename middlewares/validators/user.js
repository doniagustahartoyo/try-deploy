const validator = require("validator");
const passwordValidator = require("password-validator");
const schema = new passwordValidator();
const { user } = require("../../models");

exports.createUserValidator = async (req, res, next) => {
  try {
    const errors = [];

    //check firstname
    if (!validator.isLength(req.body.firstname, { min: 5, max: 20 })) {
      errors.push("First Name mohon diisi dengan minimal 5 karakter.");
    }

    //check lastname
    if (!validator.isLength(req.body.lastname, { min: 5, max: 20 })) {
      errors.push("Last Name mohon diisi dengan minimal 5 karakter.");
    }

    //check email
    let email = req.body.email;
    let data = await user.findOne({ where: { email: email } });
    if (data) {
      errors.push("Email ini sudah terdaftar, silahkan cari email yang lain");
    }
    function validateEmail(email) {
      const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }
    if (!validateEmail(req.body.email)) {
      errors.push("Email harus di isi dengan benar.");
    }
    // if (!isNaN(req.body.email)) {
    //   errors.push("Mohon diisi dengan Benar");
    // }

    //check password
    schema.is().min(10).is().max(20).uppercase().has().lowercase().has().digits(1);
    let password = schema.validate(req.body.password);
    if (!password) {
      errors.push("Mohon diisi dengan panjang kata sandi 10-20 karakter, terdiri dari kombinasi huruf besar, huruf kecil, angka, dan special karakter");
    }
    if (errors.length > 0) {
      return res.status(400).json({ errors: errors });
    }

    next();
  } catch (error) {
    res.status(500).json({ errors: ["Internal Server Error"] });
  }
};
