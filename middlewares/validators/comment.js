const path = require("path");
const crypto = require("crypto");
const validator = require("validator");
const { promisify } = require("util");

exports.createCommentValidator = async (req, res, next) => {
  try {
    const errors = [];
    if (!isNaN(req.body.comment)) {
      errors.push("comment must not be empty");
    }
    if (errors.length > 0) {
      res.status(400).json({ errors });
    }

    next();
  } catch (error) {
    console.log(error);
    res.status(500).json({ errors: ["Internal Server Error"] });
  }
};
