const path = require("path");
const crypto = require("crypto");
const validator = require("validator");
const { promisify } = require("util");

exports.createValidatorEvent = async (req, res, next) => {
  try {
    const errors = [];

    if (!validator.isInt(req.body.id_user)) {
      errors.push("must be a number");
    }
    if (!validator.isInt(req.body.id_speaker)) {
      errors.push("must be a number");
    }
    if (!validator.isInt(req.body.id_category)) {
      errors.push("must be integer");
    }
    if (!validator.isDate(req.body.date)) {
      errors.push("must be use YYYY/MM/DD or YYYY-MM-DD.");
    }
    if (!isNaN(req.body.title)) {
      errors.push("must not be empty");
    }
    if (!isNaN(req.body.detail)) {
      errors.push("must not be empty");
    }
    if (!isNaN(req.body.linkMeet)) {
      errors.push("must not be empty");
    }
    // If image was uploaded
    if (req.files.image) {
      // req.files.image is come from key (file) in postman
      const file = req.files.image;

      // Make sure image is photo
      if (!file.mimetype.startsWith("image")) {
        errors.push("File must be an image");
      }

      // Check file size (max 1MB)
      if (file.size > 1000000) {
        errors.push("Image must be less than 1MB");
      }

      // If error
      if (errors.length > 0) {
        return res.status(400).json({ errors: errors });
      }

      // Create custom filename
      let fileName = crypto.randomBytes(16).toString("hex");

      // Rename the file
      file.name = `${fileName}${path.parse(file.name).ext}`;

      // Make file.mv to promise
      const move = promisify(file.mv);

      // Upload image to /public/images
      await move(`./public/images/${file.name}`);

      // assign req.body.image with file.name
      req.body.image = file.name;
    }

    next();
  } catch (error) {
    console.log(error);
    res.status(500).json({ errors: ["Internal Server Error"] });
  }
};
