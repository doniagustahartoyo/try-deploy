const { decodeToken } = require("../utils/jwt");

class Auth {
  static isSignedIn(req, res, next) {
    if (!req.headers.authorization) {
      return res
        .status(401)
        .json({ statusCode: 401, message: "please sign in first" });
    }
    let token = req.headers.authorization.replace("Bearer ", "");

    let data = decodeToken(token);
    req.data = data;
    next();
  }
}

module.exports = Auth;
