const express = require("express");
const fileUpload = require("express-fileupload");
const router = require("./routes/index");

const port = process.env.PORT || 5000;

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(fileUpload());

app.use(express.static("public"));

app.use("/", router);

app.listen(port, () => console.log(`Server running on port ${port}!`));

module.exports = app;
